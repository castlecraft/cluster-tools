# Docker Buildx Bake build definition file
# Reference: https://github.com/docker/buildx/blob/master/docs/reference/buildx_bake.md

variable "CI_REGISTRY" {
    default = "registry.gitlab.com"
}
variable "CI_PROJECT_NAMESPACE" {
    default = "castlecraft"
}
variable "CI_COMMIT_TAG" {
    default = "latest"
}

target "tools" {
    context = "images"
    target = "tools"
    dockerfile = "tools.Dockerfile"
    tags = [
        "${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/cluster-tools/tools:${CI_COMMIT_TAG}",
        "${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/cluster-tools/tools:latest"
    ]
}

group "default" {
    targets = ["tools"]
}
