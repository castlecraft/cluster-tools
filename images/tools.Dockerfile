FROM debian:bookworm-slim AS tools

ARG USERNAME=castlecraft
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Install debian packages
RUN apt-get update && \
  apt-get upgrade -y && \
  apt-get install -y curl unzip sudo git zsh openssh-client && \
  # clear apt cache
  rm -rf /var/lib/apt/lists/*

# aws-cli-v2
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
  unzip awscliv2.zip && \
  ./aws/install

# kubectl
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
  mv kubectl /usr/local/bin/kubectl && \
  chmod +x /usr/local/bin/kubectl

# helm
RUN curl -fsSL https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

# aws-iam-authenticator
# RUN curl -sSL -o /usr/local/bin/aws-iam-authenticator https://github.com/kubernetes-sigs/aws-iam-authenticator/releases/download/v0.6.28/aws-iam-authenticator_0.5.5_linux_amd64 && \
#   chmod +x /usr/local/bin/aws-iam-authenticator

# argocd
RUN curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64 && \
  chmod +x /usr/local/bin/argocd

# fluxcd
RUN curl -s https://fluxcd.io/install.sh | bash

# istioctl
RUN curl -L https://istio.io/downloadIstio | TARGET_ARCH=x86_64 sh -

# eksctl
RUN curl -sLO "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_Linux_arm64.tar.gz" && \
  tar -xzf eksctl_Linux_arm64.tar.gz -C /tmp && rm eksctl_Linux_arm64.tar.gz && \
  mv /tmp/eksctl /usr/local/bin

# Add user, group, and add user to sudoer group
RUN groupadd --gid $USER_GID $USERNAME \
  && useradd --no-log-init -r -m -u $USER_UID -g $USER_GID -G sudo -s /bin/zsh $USERNAME \
  && echo "${USERNAME} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

USER ${USERNAME}

# zsh
RUN sh -c "$(curl -L https://github.com/deluan/zsh-in-docker/releases/download/v1.1.5/zsh-in-docker.sh)" -- \
  -p kubectl \
  -p helm \
  -a 'POWERLEVEL9K_DISABLE_GITSTATUS=true'

CMD [ "tail", "-f", "/dev/null" ]
