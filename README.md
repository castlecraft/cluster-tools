### Cluster Tools

- aws cli v2 / aws-iam-authenticator
- (kubectl)[https://kubernetes.io/docs/tasks/tools/#kubectl]
- (helm)[https://helm.sh]
- (argocd)[https://github.com/argoproj/argo-cd]
- (fluxcd)[https://fluxcd.io]
- (istioctl)[https://istio.io/latest/docs/setup/install/istioctl]
- (eksctl)[https://eksctl.io]
- (ohmyzsh)[https://ohmyz.sh]

Clone repo

```
git clone https:/gitlab.com/castlecraft/cluster-tools.git
```

Use with VSCode Devcontainer.

Or

Start container

```
docker-compose -p toolbox -f .devcontainer/compose.yml up -d
```

Use the container.

```
docker exec -e "TERM=xterm-256color" -w /workspace --user castlecraft -it toolbox-tools-1 bash
```

Place any files in `workspace` directory to access it within container at `/workspace`. AWS credentials will be stored in `aws` mounted at `/home/castlecraft/.aws`

Place `.ovpn` file in `vpn` directory and it will be used to connect.
